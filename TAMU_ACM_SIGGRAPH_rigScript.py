""" Simple Rig Script for Maya 2012

This script will create a simple rig based on the hierarchy defined
below and using the selected and correctly named locators.

By Stephen Aldriedge, 2012
Texas A&M University, Department of Visualization

Used and distributed with permission by
Texas A&M University ACM SIGGRAPH Student Chapter

"""

from __future__ import print_function
import maya.cmds as cmds


class Joint(object):
    """ A class containing the name of a Joint in Maya,
        a tuple of its children, whether or not the
        joint should be mirrored, and a control shape
        to attach to the Joint
    """
    def __init__(self, name, mirror, control):
        self.name = name
        self.children = ()
        self.mirror = mirror
        self.control = control


# traverse joints and run a function on each
def traverseJoints(root, func):
    func(root)
    if root.children != None:
        for child in root.children:
            traverseJoints(child, func)


# takes a list and checks if all the names are in the hierarchy
def allJointsExist(root, allJoints):
    allJointsExist.exist = True

    # in Python, function may declared within functions
    def _allJointsExist(root, allJoints):
        if root.name not in allJoints:
            allJointsExist.exist = False
            for child in root.children:
                _allJointsExist(child, allJoints)

                _allJointsExist(root, allJoints)
                return allJointsExist.exist


# create the rig from the hierarchy
def createRig(objName, root):

    # recursive function takes a parent argument and is therefore nested inside this function to hide from user
    def _createRig(root, parent=None):
        trans = cmds.getAttr('{0}.translate'.format(root.name))[0]
        cmds.delete(root.name)  # delete the locator
        cmds.joint(p=trans, name=root.name)  # create a new joint at trans
        cmds.select(d=True)  # deselect all

        if parent is not None:  # if the joint has a parent
            # connect the root joint to the parent joint
            cmds.connectJoint(root.name,
                              parent.name,
                              pm=True)

            #
            cmds.joint(parent.name,
                       edit=True,
                       zeroScaleOrient=True,
                       orientJoint='xyz')

        # create control
        if root.control is True:

            # combines root.name and CTL with an underscore between
            ctlName = '_'.join([root.name, 'CTL'])
            cmds.circle(name=ctlName, nr=(0, 1, 0), c=trans, r=2)
            cmds.pointConstraint(ctlName, root.name, mo=True)

        # recurse down to children if they exist
        for child in root.children:
            _createRig(child, root)

    cmds.group(name=objName, empty=True)  # create top level group that all nodes will be under

    scaleNUL = cmds.group(name='_'.join([objName, 'scaleNUL']),
                          empty=True,
                          parent=objName)

    posNUL = cmds.group(name='_'.join([objName, 'posNUL']),
                        empty=True,
                        parent=scaleNUL)

    rotNUL = cmds.group(name='_'.join([objName, 'rotNUL']),
                        empty=True,
                        parent=posNUL)

    mainCTL = cmds.circle(name='_'.join([objName, 'mainCTL']),
                          normal=(0, 1, 0),
                          center=(0, 0, 0),
                          radius=8)[0]  # add [0] because it returns a list

    cmds.parent(mainCTL, objName)  # parent main_CTL to the top level group

    cmds.select(d=True)  # make sure nothing is selected
    _createRig(root)  # call the recursive function with the root joint

    # mirror joints that are supposed to be mirrored
    traverseJoints(root, lambda j: (j.mirror
                                    and cmds.mirrorJoint(j.name, mxy=True)
                                    or False))

    # create group for joints
    bonesNUL = cmds.group(root.name,
                          name='_'.join([objName, 'bonesNUL']),
                          empty=True,
                          parent=mainCTL)

    # parent the root joint to the bonesNUL group
    cmds.parent(root.name, bonesNUL)


def main():

    # create joints
    hip = Joint('hip', False, 1)
    thighUpper = Joint('thighUpper', True, 0)
    knee = Joint('knee', False, 0)
    ankle = Joint('ankle', False, 1)
    toe1 = Joint('toe1', False, 0)
    toe2 = Joint('toe2', False, 0)
    neckLower = Joint('neckLower', False, 0)
    neckUpper = Joint('neckUpper', False, 0)
    head = Joint('head', False, 1)
    shoulder = Joint('shoulder', True, 0)
    elbow = Joint('elbow', False, 0)
    wrist = Joint('wrist', False, 1)
    finger1 = Joint('finger1', False, 0)
    finger2 = Joint('finger2', False, 0)

    # create hierarchy
    hip.children = (neckLower, thighUpper)
    neckLower.children = (neckUpper, shoulder)
    neckUpper.children = (head,)  # singleton (tuple with one element) must have a comma after the element
    shoulder.children = (elbow,)
    elbow.children = (wrist,)
    wrist.children = (finger1, finger2)
    thighUpper.children = (knee,)
    knee.children = (ankle,)
    ankle.children = (toe1, toe2)

    # get names of all selected transform objects
    sel = cmds.ls(selection=True, transforms=True)

    # print hierarchy
    traverseJoints(hip, lambda x: print(x.name))

    # verify that all required names are there and begin rig creation
    if allJointsExist(hip, sel):
        createRig('Man', hip)
    else:
        print('Locator names do not match the defined hierarchy.')

#------------------------------ Main ---------------------------------------
main()